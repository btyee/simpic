import os

import numpy as np

e = 1.602e-19  # C, elementary charge
me = 9.109e-31  # kg, electron mass
k = 1.381e-23  # J/K, Boltzmann constant
epsilon_0 = 8.854e-12  # F/m, permittivity of free space


class ParticleList:
    def __init__(self, config, section, rng):
        """
        Initialize a list of particles sharing certain characteristics.

        Keyword arguments:
        config -- configparser object to read from
        section -- section with species information
        """
        self.name = section[9:]
        self.q = config.getfloat(section, 'charge')
        self.m = config.getfloat(section, 'mass')
        self.w = config.getfloat(section, 'weight')
        self.repopulate = config.getfloat(section, 'repopulation_rate', fallback=0)
        self.resample = config.getboolean(section, 'resample', fallback=False)
        self.heating = config.getfloat(section, 'heating_rate', fallback=0)

        xdist = config.get(section, 'spatial_distribution')
        if xdist == 'uniform':
            xi = config.getfloat('physical', 'xi')
            xf = config.getfloat('physical', 'xf')
            def fx(num):
                return rng.uniform(xi, xf, num)
        elif xdist == 'point':
            x0 = config.getfloat(section, 'x0')
            def fx(num):
                return np.ones(num) * x0
        else:
            raise NotImplementedError(
                    '%s is not a valid spatial distribution.' % xdist)
        vdist = config.get(section, 'velocity_distribution')
        if vdist == 'uniform':
            vmin = config.get(section, 'vmin')
            vmax = config.get(section, 'vmax')
            def fv(num):
                return rng.uniform(vmin, vmax, num)
        elif vdist == 'maxwellian':
            vd = config.getfloat(section, 'drift')
            T = config.getfloat(section, 'temperature')
            sigma = (k * T / self.m)**0.5
            def fv(num):
                return rng.normal(vd, sigma, num)
        elif vdist == 'constant':
            v0 = config.getfloat(section, 'v0')
            def fv(num):
                return np.ones(num) * v0
        else:
            raise NotImplementedError(
                    '%s is not a valid velocity distribution.' % vdist)
        self.fx = fx
        self.fv = fv

        # arrays holding individual particle information
        self.x = np.array([])
        self.v = np.array([])

    def sort(self):
        ind = np.argsort(self.x) # sort from low to high, reduces memory misses later
        self.x = self.x[ind]
        self.v = self.v[ind]

    def add_particle(self, x, v):
        """
        Add individual particles to the list.

        Keyword arguments:
        x -- position (m)
        v -- velocity (m/s)
        w -- weight
        """
        self.x = np.hstack((self.x, x))
        self.v = np.hstack((self.v, v))

    def remove(self, indices):
        """
        Remove specificed particles.

        Keyword arguments:
        indices -- iterable of particle indices
        """
        self.x = np.delete(self.x, indices)
        self.v = np.delete(self.v, indices)

    def resample_particles(self, indices):
        """
        Resample the position and velocity for given particle indices.

        Keyword arguments:
        indices -- iterable of particle indices
        """
        self.x[indices] = self.fx(len(indices))
        self.v[indices] = self.fv(len(indices))

    def sample_particles(self, num):
        """
        Add new particles by sampling their distribution functions.

        Keyword arguments:
        num -- number of particles to sample
        """
        self.x = np.hstack((self.x, self.fx(num)))
        self.v = np.hstack((self.v, self.fv(num)))

    def heat_particles(self, dt, rng):
        """
        Heat particles the particles in this list.

        Keyword arguments:
        dt -- timestep size
        rng -- random number generator instance
        """
        energy = self.heating * dt
        vth = (2 * energy / (self.m * self.w))**0.5
        dv = rng.normal(0, vth, len(self.v))
        self.v += dv

    def save_descriptor(self, d):
        """
        Save basic particle information.

        Keyword arguments:
        d -- directory
        """
        fn = 'species_%s.txt' % self.name
        with open(os.path.join(d, fn), mode='w') as f:
            f.write('name: %s\nmass: %.5e\ncharge: %.5e\nweight: %.5e'
                    % (self.name, self.m, self.q, self.w))

    def save_data(self, d, step, steps, filetype='binary'):
        """
        Save particle data in specified format

        Keyword arguments:
        d -- base directory
        step -- current simulation step
        steps -- total number of steps in simulation
        filetype -- format to save as
        """
        output = np.vstack((self.x, self.v)).T
        width = int(np.ceil(np.log10(steps)))
        name = os.path.join(d, f"{step:0{width}}")
        if not os.path.exists(d):
            os.makedirs(d)
        if filetype == 'binary':
            np.save(f"{name}.npy", output)
        elif filetype == 'ascii':
            header = 'x(m),v(m/s)'
            np.savetxt(
                    f"{name}.dat", output, fmt='%.5e', delimiter=',',
                    header=header)

    def load_data(self, fn):
        """
        Load particle data from prior run.

        Keyword arguments:
        fn -- path to file to load
        """
        ext = os.path.splitext(fn)[1]
        if ext == ".dat":
            data = np.loadtxt(fn, delimiter=',')
        elif ext == ".npy":
            data = np.load(fn)
        else:
            raise IOError(f"No reader for {ext} format.")
        self.x = data[:, 0]
        self.v = data[:, 1]

