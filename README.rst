============
Introduction
============

This is intended to be a **sim**-ple particle-in-cell (**PIC**) code for
educational purposes. It was written to accompany the tutorial session on
plasma theory and modeling as part of the Gaseous Electronics Conference 2020.

In the parlance of the discipline, it is a 1D1V, planar, electrostatic
simulation bounded by fixed electric potentials (electrodes). Readability and
simplicity have been emphasized at the expense of brevity of code and the speed
with which it runs. External dependencies have been kept to a minimum,
requiring only ``numpy`` in order to obtain reasonable solution times.
Optionally, plots can be generated with ``matplotlib``.

**CAUTION**: This program is still undergoing fairly active development. Its
programming conventions have not yet stabilized and there has not yet been an
effort to rigorously verify its results.

============
Requirements
============

The only requirements for this code are,

* Python_ 3
* numpy_ >= 1.17
* matplotlib_ (optional)

and any inherited dependencies. Any reasonably recent version of these codes
should be adequate. Below are brief descriptions of how to get the necessary
software on a variety of platforms. While starting points are provided,
detailed guides are still a work in progress.

Windows
-------

The easiest approach to satisfying the above dependencies is to download an
existing distribution of python. Without endorsing a specific one, there are
several options:

* `Anaconda (Individual Edition)`_
* `Enthought Deployment Manager`_
* `Python(x,y)`_

As an alternative, one may use Cygwin_ in order to obtain a POSIX-like
environment under Windows. However, this option requires some knowledge of
building software and isn't recommended for those not already familiar with
the process.

MacOS
-----

Unfortunately, MacOS ships with an older version of Python (2.7) which prevents
it from being used to run ``simpic``. The easiest approach to meeting the above
requirements involves using a third-party package manager such as

* MacPorts_
* Homebrew_

In particular, MacPorts maintains builds of both numpy and matplotlib that can
be easily installed.

Linux
-----

If you're running Linux, the best approach would be to use your distribution's
package manager as much as possible and ``pip`` when necessary.


===============
Getting Started
===============

To get the code, you can download simpic_ at its website or, if you have ``git``
installed, you can clone it with the command:

::

$ git clone https://gitlab.com/btyee/simpic.git

The file, `simpic`, is the main Python script for the program. You can open and
edit it to your heart's content. A set of different models are available in the
directory `models` which must be selected at runtime. To write your own model
see the existing configuration files for reference.

::

$ ./simpic [model configuration file]

All of the output will be written to the folder `output` and saved, by default,
in numpy's binary format. After the simulation is complete, feel free to use
some of the scripts in `visualization` to plot figures of the results.

=================
Funding Statement
=================
*Sandia National Laboratories is a multimission laboratory managed and operated 
by National Technology and Engineering Solutions of Sandia, LLC, a wholly owned 
subsidiary of Honeywell International Inc., for the U.S. Department of Energy’s 
National Nuclear Security Administration under contract DE-NA0003525.*

.. _matplotlib: https://matplotlib.org/
.. _Python: https://www.python.org/
.. _numpy: https://numpy.org/
.. _`Anaconda (Individual Edition)`: https://anaconda.org/anaconda/python
.. _`Enthought Deployment Manager`: https://assets.enthought.com/downloads/edm
.. _`Python(x,y)`: https://python-xy.github.io/
.. _Cygwin: https://www.cygwin.com/
.. _MacPorts: https://www.macports.org/
.. _Homebrew: https://brew.sh/
.. _simpic: https://gitlab.com/btyee/simpic
