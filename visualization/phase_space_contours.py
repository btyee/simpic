#!/usr/bin/env python3

import numpy as np

def read_species_info(fn):
    d = {}
    types = {'name': str, 'mass': float, 'charge': float, 'weight': float}
    with open(fn, mode='r') as f:
        for line in f:
            k, v = line.split(': ')
            d[k] = types[k](v)
    return d

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(description="Plot contours of phase space")
    parser.add_argument('experiment', type=str, help='name of the simulation')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite any pre-existing files')
    parser.add_argument(
            '--lookahead', '-l', action='store_true', help='Scan all results '
            'to find appropriate y-limits.')
    parser.add_argument(
            '--vbins', '-v', type=int, default=100, help='Number of velocity '
            'bins to use.')
    parser.add_argument(
            '--xbins', '-x', type=int, default=100, help='Number of spatial '
            'bins to use.')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment, 'particles')
    datadir = os.path.join('output', args.experiment)
    particledir = os.path.join('output', args.experiment, 'particles')

    if args.force:
        action = 'o'
    elif os.path.exists(visdir):
        while True:
            action = input(
                    'Existing figures found: [o]verwrite, [d]elete, [a]bort, '
                    'or [s]kip existing? ')
            if action == 'o' or action == 's':
                break
            elif action == 'd':
                from shutil import rmtree
                for p in os.listdir(visdir):
                    if p != 'particles':
                        rmtree(os.path.join(visdir, p))
                break
            elif action == 'a':
                import sys
                sys.exit()
    else:
        action = 'o'

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt
    cells = config.getint("simulation", "cells")
    xi = config.getfloat("physical", "xi")
    xf = config.getfloat("physical", "xf")
    spatial_grid = np.linspace(xi, xf, num=cells+1)

    for dirpath, dirnames, filenames in os.walk(particledir):
        if dirpath == particledir:
            continue
        suffix = os.path.split(os.path.relpath(dirpath, datadir))[1]
        print(suffix)
        species = read_species_info(os.path.join(datadir, f'species_{suffix}.txt'))
        outputdir = os.path.join(visdir, suffix)
        try:
            os.makedirs(outputdir)
        except IOError:
            pass
        filenames.sort(key=lambda i: int(i[:-4]))
        computational_count = np.zeros(len(filenames))
        steps = np.zeros(len(filenames), dtype='int64')
        if args.lookahead:
            print('\tGetting velocity bounds')
            vmin, vmax = 0, 0
            for fn in filenames:
                data = np.load(os.path.join(dirpath, fn))
                try:
                    vmin = min((vmin, data[:, 1].min()))
                    vmax = max((vmax, data[:, 1].max()))
                except ValueError:
                    # no particles present
                    continue
            zmax = 0
            xbins = np.linspace(spatial_grid[0], spatial_grid[-1], args.xbins)
            vbins = np.linspace(vmin, vmax, args.xbins)
            print('\tGetting density bounds')
            for fn in filenames:
                data = np.load(os.path.join(dirpath, fn))
                H, _, _ = np.histogram2d(
                        data[:, 0], data[:, 1], bins=(xbins, vbins),
                        weights=species['weight']*np.ones(data.shape[0]))
                zmax = np.max((zmax, H.max()))
        print('\tPlotting')
        for i, fn in enumerate(filenames):
            name = os.path.splitext(fn)[0]
            steps[i] = int(name)
            data = np.load(os.path.join(dirpath, fn))
            savepath = os.path.join(outputdir, name + '.png')
            if action == 's' and os.path.exists(savepath):
                continue
            x = data[:, 0]
            v = data[:, 1]
            fig, ax = plt.subplots(1, 1)
            xmargin = (spatial_grid.max() - spatial_grid.min()) * 0.05
            ax.set_xlim(
                    1e2*(spatial_grid.min() - xmargin),
                    1e2*(spatial_grid.max() + xmargin))
            ax.axvspan(
                    1e2*(spatial_grid.min() - xmargin), 1e2*spatial_grid.min(),
                    facecolor='black', alpha=0.3)
            ax.axvspan(
                    1e2*spatial_grid.max(), 1e2*(spatial_grid.max() + xmargin),
                    facecolor='black', alpha=0.3)
            if args.lookahead:
                H, _, _, im = ax.hist2d(
                        1e2*x, v, bins=(1e2*xbins, vbins), vmin=0, vmax=zmax,
                        weights=species['weight']*np.ones(len(x)))
            else:
                H, xedges, yedges, im =ax.hist2d(
                        1e2*x, v, bins=(100*args.xbins, args.vbins),
                        weights=species['weight']*np.ones(len(x)))
                ax.set_title(f"t = {temporal_grid[steps[i]]:.2e} s")
            ax.set_xlabel('Distance (cm)')
            ax.set_ylabel('Velocity (m/s)')
            cbar = fig.colorbar(im, ax=ax)
            cbar.set_label(f"Number of {suffix}s")
            fig.tight_layout()
            fig.savefig(savepath)
            plt.close(fig)
