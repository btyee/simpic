#!/usr/bin/env python3

import numpy as np

def load_field_data(fn):
    data = np.load(fn)
    return data

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(description='Plot the field quantities of a simulation')
    parser.add_argument('experiment', type=str, help='name of the simulation')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite any pre-existing files')
    parser.add_argument(
            '--lookahead', '-l', action='store_true', help='Scan all results '
            'to find appropriate y-limits.')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment)
    datadir = os.path.join('output', args.experiment)

    if args.force:
        action = 'o'
    elif os.path.exists(visdir):
        while True:
            action = input(
                    'Existing figures found: [o]verwrite, [d]elete, [a]bort, '
                    'or [s]kip existing? ')
            if action == 'o' or action == 's':
                break
            elif action == 'd':
                from shutil import rmtree
                for p in os.listdir(visdir):
                    if p != 'particles':
                        rmtree(os.path.join(visdir, p))
                break
            elif action == 'a':
                import sys
                sys.exit()
    else:
        action = 'o'

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt
    cells = config.getint("simulation", "cells")
    xi = config.getfloat("physical", "xi")
    xf = config.getfloat("physical", "xf")
    spatial_grid = np.linspace(xi, xf, num=cells+1)

    for dirpath, dirnames, filenames in os.walk(datadir):
        if dirpath == datadir or 'particles' in dirpath or len(filenames) == 0:
            continue
        else:
            suffix = os.path.relpath(dirpath, datadir)
            print(suffix)
            outputdir = os.path.join(visdir, suffix)
            try:
                os.makedirs(outputdir)
            except IOError:
                pass
            filenames.sort(key=lambda i: int(i[:-4]))
            if args.lookahead:
                ymin, ymax = 0, 0
                for fn in filenames:
                    data = np.load(os.path.join(dirpath, fn))
                    ymin = min((ymin, data.min()))
                    ymax = max((ymax, data.max()))
            for fn in filenames:
                name = os.path.splitext(fn)[0]
                step = int(name)
                savepath = os.path.join(outputdir, name + '.png')
                if action == 's' and os.path.exists(savepath):
                    continue
                data = np.load(os.path.join(dirpath, fn))
                if not ("electric_field" in suffix) or not ("electric_potential" in suffix):
                    # hack to get around weird velocity/temperatures at system edges,
                    # problem with simulations?
                    x = spatial_grid[2:-2]
                    data = data[2:-2]
                else:
                    x = spatial_grid
                fig, ax = plt.subplots(1, 1)
                if len(x) == len(data):
                    ax.plot(x, data)
                else:
                    ax.plot((x[1:] + x[:-1])/2, data)
                xmargin = (x.max() - x.min()) * 0.05
                ax.axvspan(x.min() - xmargin, x.min(), facecolor='black', alpha=0.3)
                ax.axvspan(x.max(), x.max() + xmargin, facecolor='black', alpha=0.3)
                ax.set_xlim(x.min() - xmargin, x.max()+xmargin)
                ax.set_title('t = {temporal_grid[step]:.2e} s')
                ax.set_ylabel(suffix)
                ax.set_xlabel('Distance (m)')
                if args.lookahead:
                    ax.set_ylim(1.05*ymin, 1.05*ymax)
                fig.tight_layout()
                fig.savefig(savepath)
                plt.close(fig)
