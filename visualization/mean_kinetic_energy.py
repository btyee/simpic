import os
import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np

e = 1.602e-19

plt.style.use('visualization/presentation.mplstyle')

def read_species_info(fn):
    d = {}
    types = {'name': str, 'mass': float, 'charge': float, 'weight': float}
    with open(fn, mode='r') as f:
        for line in f:
            k, v = line.split(': ')
            d[k] = types[k](v)
    return d

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    parser = argparse.ArgumentParser(description="Plot mean energy of the particles over time")
    parser.add_argument('experiment', type=str, help='name of the simulation')
    args = parser.parse_args()

    datadir = os.path.join('output', args.experiment)
    particledir = os.path.join(datadir, 'particles')
    visdir = os.path.join('figures', args.experiment)

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt

    try:
        os.makedirs(visdir)
    except IOError:
        pass

    for dirpath, dirnames, filenames in os.walk(particledir):
        if dirpath == particledir:
            continue
        suffix = os.path.split(os.path.relpath(dirpath, particledir))[1]
        print(suffix)

        species = read_species_info(os.path.join(datadir, 'species_%s.txt' % suffix))

        filenames = os.listdir(os.path.join(particledir, suffix))
        filenames.sort(key=lambda p: int(p[:-4]))

        t = np.zeros(len(filenames))
        E = np.zeros(len(filenames))
        C = np.zeros(len(filenames))

        for i, fn in enumerate(filenames):
            step = int(fn[:-4])
            t[i] = temporal_grid[step]
            data = np.load(os.path.join(particledir, suffix, fn))
            v = data[:, 1]
            E[i] = np.sum(0.5 * species['mass'] * v**2)
            C[i] = len(v)

        mean_energy = np.nan_to_num(E/C, posinf=0, neginf=0)

        fig, ax = plt.subplots(1, 1)
        ax.plot(1e6*t, mean_energy/e)
        ax.set_xlabel('Time ($\mu$s)')
        ax.set_ylabel('Mean Energy (eV)')
        fig.tight_layout()
        fig.savefig(os.path.join(visdir, 'mean_energy_%s.png' % suffix))
