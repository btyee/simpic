#!/usr/bin/env python3

import numpy as np

def load_field_data(fn):
    data = np.load(fn)
    return data

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(
            description='Plot averages of field quantities for a simulation')
    parser.add_argument('experiment', type=str, help='Name of the simulation')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite any pre-existing files')
    parser.add_argument('--tmin', type=float, default=None, help='starting time')
    parser.add_argument('--tmax', type=float, default=None, help='stopping time')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment)
    datadir = os.path.join('output', args.experiment)

    if args.force:
        action = 'o'
    elif os.path.exists(visdir):
        while True:
            action = input(
                    'Existing figures found: [o]verwrite, [d]elete, [a]bort, '
                    'or [s]kip existing? ')
            if action == 'o' or action == 's':
                break
            elif action == 'd':
                from shutil import rmtree
                for p in os.listdir(visdir):
                    if p != 'particles':
                        rmtree(os.path.join(visdir, p))
                break
            elif action == 'a':
                import sys
                sys.exit()
    else:
        action = 'o'

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt
    cells = config.getint("simulation", "cells")
    xi = config.getfloat("physical", "xi")
    xf = config.getfloat("physical", "xf")
    spatial_grid = np.linspace(xi, xf, num=cells+1)
    
    for dirpath, dirnames, filenames in os.walk(datadir):
        if dirpath == datadir or 'particles' in dirpath or len(filenames) == 0:
            continue
        suffix = os.path.relpath(dirpath, datadir)
        print(suffix)
        outputdir = os.path.join(visdir, suffix)
        try:
            os.makedirs(outputdir)
        except IOError:
            pass
        filenames.sort(key=lambda i: int(i[:-4]))
        filenames = np.array(filenames)
        steps = np.array([int(fn[:-4]) for fn in filenames])
        available_times = temporal_grid[steps]
        ind = np.array([True] * len(available_times))
        if args.tmin is not None:
            ind = ind * (available_times >= args.tmin)
        if args.tmax is not None:
            ind = ind * (available_times <= args.tmax)
        filenames = filenames[ind]
        times = available_times[ind]
        lenx = np.load(os.path.join(dirpath, filenames[0])).shape[0]
        if lenx == len(spatial_grid):
            x = spatial_grid
        elif lenx == len(spatial_grid) - 1:
            x = (spatial_grid[1:] + spatial_grid[:-1]) / 2
        data = np.zeros((len(times), len(x)))
        for i, (t, fn) in enumerate(zip(times, filenames)):
            if action == 's' and os.path.exists(visdir):
                continue
            data[i, :] = np.load(os.path.join(dirpath, fn))
        y = data.mean(axis=0)
        if not ("electric_field" in suffix) or not ("electric_potential" in suffix):
            # hack to get around weird velocity/temperatures at system edges,
            # problem with simulations?
            y = y[2:-2]
            x = x[2:-2]
        fig, ax = plt.subplots(1, 1)
        ax.plot(1e2*x, y)
        ax.set_xlabel('Distance (cm)')
        ax.set_ylabel(suffix)
        fig.tight_layout()
        fig.savefig(os.path.join(visdir, f'{suffix}_average.png'))
        plt.close(fig)
