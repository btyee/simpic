import os

import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('visualization/presentation_quarter.mplstyle')

from scipy.constants import e, epsilon_0, pi

datadir = os.path.join('output', 'constant_acceleration', 'particles', 'electron')
filenames = os.listdir(datadir)
filenames = [fn for fn in filenames if fn.endswith('.npy')]
filenames = [fn for fn in filenames if not fn.startswith('.')]
filenames.sort()

x = np.zeros(len(filenames))
v = np.zeros(len(filenames))
w = np.zeros(len(filenames))

for i, fn in enumerate(filenames):
    x[i], v[i] = np.load(os.path.join(datadir, fn))[0]

V = 1
L = 1e-2
E = V/L
from scipy.constants import e, m_e
xa = np.linspace(0, L, num=1000)
va = (2 * e * E * xa / m_e)**0.5

fig, ax = plt.subplots(1, 1)
ax.plot(1e2*x, 1e-4*v, label='Simulated')
ax.plot(1e2*xa, 1e-4*va, label='Analytic')
ax.set_xlabel('Distance (cm)')
ax.set_ylabel('Velocity (cm/$\mu$s)')
ax.legend()
fig.tight_layout()
fig.savefig('figures/constant_acceleration/phase_space.pdf')
