import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np

e = 1.602e-19
epsilon_0 = 8.854e-12

plt.style.use('visualization/presentation.mplstyle')

x = np.loadtxt('output/single_particle_field/spatial_grid.dat')
Es = np.load('output/single_particle_field/electric_field/0.npy')
Vs = np.load('output/single_particle_field/electric_potential/0.npy')

xj = (x[-1] - x[0]) / 2
Ea = -e * (x - xj) / (2*epsilon_0 * (np.abs(x - xj)))
Va = np.zeros(len(Ea))
Va = np.abs(xj - x) * e / (2 * epsilon_0)
Va -= Va.max()

fig, axes = plt.subplots(2, 1, sharex=True)
axes[0].plot(1e2*x, Es, label='Simulated')
axes[0].plot(1e2*x, Ea, label='Analytic')
axes[0].set_ylabel('Electric Field (V/m)')
axes[1].plot(1e2*x, Vs, label='Simulated')
axes[1].plot(1e2*x, Va, label='Analytic')
#axes[1].legend()
axes[1].set_xlabel('Distance (cm)')
axes[1].set_ylabel('Potential (V)')

fig.tight_layout()
fig.savefig('figures/single_particle_field/analytic_comparison.pdf')
