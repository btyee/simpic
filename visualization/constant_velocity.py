import os

import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('visualization/presentation_quarter.mplstyle')

from scipy.constants import e, epsilon_0, pi

datadir = os.path.join('output', 'particle_crossing', 'particles', 'electron')
filenames = os.listdir(datadir)
filenames = [fn for fn in filenames if fn.endswith('.npy')]
filenames = [fn for fn in filenames if not fn.startswith('.')]
filenames.sort()

x = np.zeros(len(filenames))
v = np.zeros(len(filenames))
w = np.zeros(len(filenames))

for i, fn in enumerate(filenames):
    x[i], v[i], w[i] = np.load(os.path.join(datadir, fn))[0]

fig, ax = plt.subplots(1, 1)
ax.plot(1e2*x, 1e-4*v, label='Simulated')
ax.plot(1e2*x, 1e2 * np.ones(len(x)), label='Analytic')
#ax.set_ylim(-2e6, 2e6)
ax.set_ylim(0, 110)
ax.set_xlabel('Distance (cm)')
ax.set_ylabel('Velocity (cm/$\mu$s)')
#ax.legend()
fig.tight_layout()
fig.savefig('figures/particle_crossing/phase_space.pdf')
