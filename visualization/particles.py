#!/usr/bin/env python3

import numpy as np

def read_species_info(fn):
    d = {}
    types = {'name': str, 'mass': float, 'charge': float, 'weight': float}
    with open(fn, mode='r') as f:
        for line in f:
            k, v = line.split(': ')
            d[k] = types[k](v)
    return d

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(description="Plot the particles' phase space")
    parser.add_argument('experiment', type=str, help='name of the simulation')
    parser.add_argument(
            '--lookahead', '-l', action='store_true', help='Scan all results '
            'to find appropriate y-limits.')
    parser.add_argument(
            '--alpha', '-a', type=float, default=1, help='Transparency to use '
            'when plotting particle markers.')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment, 'particles')
    datadir = os.path.join('output', args.experiment)
    particledir = os.path.join('output', args.experiment, 'particles')

    if os.path.exists(visdir):
        while True:
            action = input(
                    'Existing figures found: [o]verwrite, [d]elete, [a]bort, '
                    'or [s]kip existing? ')
            if action == 'o' or action == 's':
                break
            elif action == 'd':
                from shutil import rmtree
                rmtree(visdir)
                break
            elif action == 'a':
                import sys
                sys.exit()
    else:
        action = 'o'

    #t = np.loadtxt(os.path.join(datadir, 'temporal_grid.dat'))
    #grid = np.loadtxt(os.path.join(datadir, 'spatial_grid.dat'))

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt

    for dirpath, dirnames, filenames in os.walk(particledir):
        if dirpath == particledir:
            continue
        suffix = os.path.split(os.path.relpath(dirpath, datadir))[1]
        print(suffix)
        species = read_species_info(os.path.join(datadir, 'species_%s.txt' % suffix))
        outputdir = os.path.join(visdir, suffix)
        try:
            os.makedirs(outputdir)
        except IOError:
            pass
        filenames.sort(key=lambda i: int(i[:-4]))
        computational_count = np.zeros(len(filenames))
        steps = np.zeros(len(filenames), dtype='int64')
        if args.lookahead:
            vmin, vmax = 0, 0
            for fn in filenames:
                data = np.load(os.path.join(dirpath, fn))
                try:
                    vmin = min((vmin, data[:, 1].min()))
                    vmax = max((vmax, data[:, 1].max()))
                except ValueError:
                    # no particles present
                    continue
        for i, fn in enumerate(filenames):
            name = os.path.splitext(fn)[0]
            steps[i] = int(name)
            data = np.load(os.path.join(dirpath, fn))
            computational_count[i] = data.shape[0]
            savepath = os.path.join(outputdir, name + '.png')
            if action == 's' and os.path.exists(savepath):
                continue
            x = data[:, 0]
            v = data[:, 1]
            fig, ax = plt.subplots(1, 1)
            ax.set_xlim(0.485, 0.515)
            ax.scatter(1e2*x, v, alpha=args.alpha)
            ax.set_title(f't = {temporal_grid[steps[i]]:.2e} s')
            ax.set_xlabel('Distance (cm)')
            ax.set_ylabel('Velocity (m/s)')
            if args.lookahead:
                ymargin = (vmax - vmin) * 0.05
                ax.set_ylim(vmin - ymargin, vmax + ymargin)
            fig.tight_layout()
            fig.savefig(savepath)
            plt.close(fig)
        fig, ax = plt.subplots(1, 1)
        ax.plot(t[steps], computational_count)
        ax2 = ax.twinx()
        physical_count = computational_count * species['weight']
        ax2.plot(t[steps], physical_count)
        ax.set_ylabel('Computational Particle Count')
        ax2.set_ylabel('Physical Particle Count')
        ax.set_xlabel('Time (s)')
        fig.tight_layout()
        fig.savefig(os.path.join(visdir, '%s_count.png' % suffix))
        plt.close(fig)
