#!/usr/bin/env python3

if __name__ == '__main__':
    import argparse
    import os

    parser = argparse.ArgumentParser(description="Animate particle and field data.")
    parser.add_argument('experiment', type=str, help='name of the simulation')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment)
    cmd = [
            "ffmpeg", "-framerate", "24", "-pattern_type", "glob", "-i", "'%s*.png'", -pix_fmt yuv420p %s"
            ffmpeg -framerate 24 -pattern_type glob -i '%s*.png' -pix_fmt yuv420p %s

    for dirpath, dirnames, filenames in os.walk(visdir):
        if dirpath == visdir or len(filenames) == 0:
            continue
        print(dirpath)

