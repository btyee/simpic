#!/usr/bin/env python3

import numpy as np

def load_field_data(fn):
    data = np.load(fn)
    return data

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(
            description='Plot contours of field quantities for a simulation')
    parser.add_argument('experiment', type=str, help='Name of the simulation')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite any pre-existing files')
    parser.add_argument(
            '--samples', '-s', type=int, default=200, help="Number of time "
            "slices to use")
    parser.add_argument(
            '--logscale', '-l', action='store_true', help='Pick samples on a '
            'log scale in time')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment)
    datadir = os.path.join('output', args.experiment)

    if args.force:
        action = 'o'
    elif os.path.exists(visdir):
        while True:
            action = input(
                    'Existing figures found: [o]verwrite, [d]elete, [a]bort, '
                    'or [s]kip existing? ')
            if action == 'o' or action == 's':
                break
            elif action == 'd':
                from shutil import rmtree
                for p in os.listdir(visdir):
                    if p != 'particles':
                        rmtree(os.path.join(visdir, p))
                break
            elif action == 'a':
                import sys
                sys.exit()
    else:
        action = 'o'

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt
    cells = config.getint("simulation", "cells")
    xi = config.getfloat("physical", "xi")
    xf = config.getfloat("physical", "xf")
    spatial_grid = np.linspace(xi, xf, num=cells+1)

    if temporal_grid.shape == ():
        temporal_grid = np.reshape(temporal_grid, (1, ))

    for dirpath, dirnames, filenames in os.walk(datadir):
        if dirpath == datadir or 'particles' in dirpath or len(filenames) == 0:
            continue
        suffix = os.path.relpath(dirpath, datadir)
        print(suffix)
        outputdir = os.path.join(visdir, suffix)
        try:
            os.makedirs(outputdir)
        except IOError:
            pass
        filenames.sort(key=lambda i: int(i[:-4]))
        filenames = np.array(filenames)
        steps = np.array([int(fn[:-4]) for fn in filenames])
        if len(filenames) < args.samples:
            print('Insufficient data, using all available output.')
            args.samples = len(filenames)
            times = temporal_grid[steps]
        else:
            available_times = temporal_grid[steps]
            if args.logscale:
                desired_times = np.logspace(
                        np.log10(available_times[1]), np.log10(available_times[-1]), num=args.samples)
            else:
                desired_times = np.linspace(available_times[0], available_times[-1], num=args.samples)
            ind = np.zeros(args.samples, dtype='int64')
            for i, t in enumerate(desired_times):
                ind[i] = np.argmin(np.abs(available_times - t))
            filenames = filenames[ind]
            times = available_times[ind]
        lenx = np.load(os.path.join(dirpath, filenames[0])).shape[0]
        if lenx == len(spatial_grid):
            x = spatial_grid
        elif lenx == len(spatial_grid) - 1:
            x = (spatial_grid[1:] + spatial_grid[:-1]) / 2
        data = np.zeros((len(times), len(x)))
        for i, (t, fn) in enumerate(zip(times, filenames)):
            if action == 's' and os.path.exists(visdir):
                continue
            data[i, :] = np.load(os.path.join(dirpath, fn))
        if not ("electric_field" in suffix) or not ("electric_potential" in suffix):
            # hack to get around weird velocity/temperatures at system edges,
            # problem with simulations?
            data = data[:, 2:-2]
            x = x[2:-2]
        fig, ax = plt.subplots(1, 1)
        if data.min() < 0 and data.max() > 0 and -data.min() > 0.1*data.max():
            cmap = 'RdBu_r'
        else:
            cmap = None
        # remove outliers
        sigma = data.std()
        mu = data.mean()
        positive_only = ("densities", "temperatures")
        vmax = mu + (6 * sigma)
        if any([i in suffix for i in positive_only]):
            vmin = 0
        else:
            vmin = mu - (6 * sigma)
        pcm = ax.pcolormesh(1e2*x, 1e6*times, data, vmin=vmin, vmax=vmax, cmap=cmap, shading='auto')
        if args.logscale:
            ax.set_yscale('log')
        ax.set_xlabel('Distance (cm)')
        ax.set_ylabel('Time ($\mu$s)')
        ax.set_title(suffix)
        fig.colorbar(pcm, ax=ax)
        fig.tight_layout()
        fig.savefig(os.path.join(visdir, '%s_contour.png' % suffix))
        plt.close(fig)
