#!/usr/bin/env python3

import numpy as np

def read_species_info(fn):
    d = {}
    types = {'name': str, 'mass': float, 'charge': float, 'weight': float}
    with open(fn, mode='r') as f:
        for line in f:
            k, v = line.split(': ')
            d[k] = types[k](v)
    return d

if __name__ == '__main__':
    import argparse
    import configparser
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(description="Count total particles in system")
    parser.add_argument('experiment', type=str, help='name of the simulation')
    args = parser.parse_args()

    visdir = os.path.join('figures', args.experiment, 'particles')
    datadir = os.path.join('output', args.experiment)
    particledir = os.path.join('output', args.experiment, 'particles')

    fn_config = [i for i in os.listdir(datadir) if i.endswith(".cfg")]
    if len(fn_config) > 1:
        raise IOError(f"Multiple configuration files detected: {fn_config}")
    config = configparser.ConfigParser()
    config.read(os.path.join(datadir, fn_config[0]))
    dt = config.getfloat("simulation", "dt")
    steps = config.getint("simulation", "steps")
    temporal_grid = np.arange(steps) * dt

    for dirpath, dirnames, filenames in os.walk(particledir):
        if dirpath == particledir:
            continue
        suffix = os.path.split(os.path.relpath(dirpath, datadir))[1]
        print(suffix)
        species = read_species_info(os.path.join(datadir, 'species_%s.txt' % suffix))
        outputdir = os.path.join(visdir, suffix)
        try:
            os.makedirs(outputdir)
        except OSError:
            pass
        filenames.sort(key=lambda i: int(i[:-4]))
        computational_count = np.zeros(len(filenames))
        steps = np.zeros(len(filenames), dtype='int64')
        for i, fn in enumerate(filenames):
            name = os.path.splitext(fn)[0]
            steps[i] = int(name)
            data = np.load(os.path.join(dirpath, fn))
            computational_count[i] = data.shape[0]
        fig, ax = plt.subplots(1, 1)
        ax.plot(temporal_grid[steps], computational_count)
        ax2 = ax.twinx()
        physical_count = computational_count * species['weight']
        ax2.plot(1e6*temporal_grid[steps], physical_count)
        ax.set_ylabel('Computational Particle Count')
        ax2.set_ylabel('Physical Particle Count')
        ax.set_xlabel('Time ($\mu$s)')
        fig.tight_layout()
        fig.savefig(os.path.join(visdir, '%s_count.png' % suffix))
        plt.close(fig)
