import os

import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np

plt.style.use('visualization/presentation.mplstyle')


if __name__ == "__main__":
    import argparse
    import os

    import matplotlib; matplotlib.use('agg')
    import matplotlib.pyplot as plt

    plt.style.use('visualization/presentation.mplstyle')

    parser = argparse.ArgumentParser(
            description='Plot averages of field quantities for a simulation')
    parser.add_argument('experiment', type=str, help='Name of the simulation')
    parser.add_argument('--force', '-f', action='store_true', help='overwrite any pre-existing files')
    parser.add_argument('--tmin', type=float, default=None, help='starting time')
    parser.add_argument('--tmax', type=float, default=None, help='stopping time')
    parser.add_argument('--interval', '-i', type=int, default=1, help='Stride interval for time sampling')
    args = parser.parse_args()

    output = os.path.join('figures', args.experiment)
    L = 10e-2
    regions = [
            ('Presheath', 4e-2, 5e-2),
            ('Sheath Edge', 0.3e-2, 1.3e-2)
    ]
    
    # manually get these from run information
    from scipy.constants import e, epsilon_0, k, m_e, atomic_mass
    n = 1.4e16
    T = 4.0 * e/k
    Ld = np.sqrt(epsilon_0 * k * T / (n * e**2))
    f1 = 0.42
    n1 = f1 * n
    n2 = (1 - f1) * n
    cs1 = np.sqrt(k * T / (4.002602 * atomic_mass))  # helium
    cs2 = np.sqrt(k * T / (131.293 * atomic_mass))  # xenon
    cs = np.sqrt(n1*cs1**2 + n2*cs2**2)



    
    temporal_grid = np.loadtxt(os.path.join('output', args.experiment, 'temporal_grid.dat'))
    x = np.loadtxt(os.path.join('output', args.experiment, 'spatial_grid.dat'))
    cells = (x[1:] + x[:-1]) / 2
    
    densitydir =  os.path.join('output', args.experiment, 'densities')
    for species in os.listdir(densitydir):
        datadir = os.path.join(densitydir, species)
        filenames = os.listdir(datadir)
        filenames.sort(key=lambda i: int(i[:-4]))
        filenames = np.array(filenames)
        t = np.array([temporal_grid[int(fn[:-4])] for fn in filenames])
        tmask = (t >= args.tmin) * (t <= args.tmax)
        filenames = filenames[tmask]
        t = t[tmask]
        
        filenames = filenames[::args.interval]
        t = t[::args.interval]
        
        densities = []
        xmasks = []
        for region, xmin, xmax in regions:
            xmask = (cells >= xmin) * (cells <= xmax)
            xmasks.append(xmask)
            density = np.zeros((len(t), np.sum(xmask)))
            densities.append(density)
        
        for i, fn in enumerate(filenames):
            results = np.load(os.path.join(datadir, fn))
            for j in range(len(regions)):
                densities[j][i, :] = results[xmasks[j]]
        
        fig, axes = plt.subplots(1, len(regions), sharey=True)
        for i, (region, xmin, xmax) in enumerate(regions):
            dnn = (densities[i] - densities[i].mean())/densities[i].mean()
            pcm = axes[i].pcolormesh(cells[xmasks[i]], t, dnn, shading='auto', cmap='RdBu_r')
            axes[i].set_xlabel('Distance (m)')
            axes[i].set_title(region)
            cbar = fig.colorbar(pcm, ax=axes[i])
            cbar.set_label('$\delta n$/n')
        axes[0].set_ylabel('Time (s)')
        fig.set_size_inches(10, 4)
        fig.tight_layout()
        fig.savefig(os.path.join(output, '%s_density_delta_contour.png' % species))
        plt.close(fig)
        
        ffts = []
        for density in densities:
            ffts.append(np.abs(np.fft.fft2(density)))
        
        # for the FFTs, also get the other side as well
        densities = []
        xmasks = []
        for region, xmin, xmax in regions:
            xmask = (cells >= xmin) * (cells <= xmax)
            xmasks.append(xmask)
            density = np.zeros((len(t), np.sum(xmask)))
            densities.append(density)
        
        dx = cells[1] - cells[0]
        dt = t[1] - t[0]
        f = np.fft.fftfreq(len(t), d=dt)
        f = np.fft.fftshift(f)
        
        fig, axes = plt.subplots(1, len(regions), sharey=True)
        for i, (region, xmin, xmax) in enumerate(regions):
            l = np.fft.fftfreq(len(cells[xmasks[i]]), d=dx)
            l = np.fft.fftshift(l)
            ffts[i] = np.fft.fftshift(ffts[i])
            pcm = axes[i].pcolormesh(l * Ld, f / (cs*Ld), np.log(ffts[i]), shading='auto', vmin=38, vmax=44)
            axes[i].set_xlabel('Wavenumber (k$\lambda_D$)')
            axes[i].set_title(region)
            axes[i].set_xlim(-3, 3)
            cbar = fig.colorbar(pcm, ax=axes[i])
            cbar.set_label(r'log($\tilde{n}$)')
        axes[0].set_ylabel('Frequency ($\omega$/($c_s$/$\lambda_D$))')
        fig.set_size_inches(12, 5)
        fig.tight_layout()
        fig.savefig(os.path.join(output, '%s_frequency_analysis.png' % species))
        plt.close(fig)
